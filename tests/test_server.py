import shutil
import unittest
from xml.etree import ElementTree

from server import PaymentServer

EXPECTED_RESULT = (
    ("ACCEPTED", "None"),
    ("DECLINED", "InsufficientFunds"),
    ("DECLINED", "TransactionAmountOverLimit"),
    ("DECLINED", "TransactionCountOverLimit"),
    ("ACCEPTED", "None"),
    ("ACCEPTED", "None"),
    ("ACCEPTED", "None"),
    ("DECLINED", "TransactionAmountOverLimit"),
    ("ACCEPTED", "None"),
    ("ACCEPTED", "None"),
    ("DECLINED", "ItsRaining"),
    ("DECLINED", "ItsRaining"),
    ("DECLINED", "InsufficientFunds"),
    ("DECLINED", "ItsRaining"),
    ("DECLINED", "None"),
    ("DECLINED", "InsufficientFunds"),
    ("ACCEPTED", "None"),
    ("ACCEPTED", "None"),
    ("DECLINED", "TransactionCountOverLimit"),
    ("DECLINED", "TransactionCountOverLimit"),
)


class PaymentServerTestCase(unittest.TestCase):
    def setUp(self) -> None:
        shutil.copy("../src/resources/limits", "../src/resources/db")
        open("../src/resources/transactions.csv", "w").close()
        self.ps = PaymentServer()

    def test_series_handle(self):
        for payment_no in range(20):
            with open(
                f"../src/resources/payments/payment_{payment_no+1}.xml", "r"
            ) as request:
                response = self.ps.handle(request.read())
                response_xml = ElementTree.fromstring(response)
            self.assertEqual(
                (
                    response_xml.find("TransactionResponse").find("Result").text,
                    response_xml.find("TransactionResponse").find("Reason").text,
                ),
                EXPECTED_RESULT[payment_no],
            )
