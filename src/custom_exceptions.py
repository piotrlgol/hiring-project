class PaymentException(Exception):
    def __str__(self):
        return "None"


class InsufficientFunds(PaymentException):
    def __str__(self):
        return "InsufficientFunds"


class TransactionAmountOverLimit(PaymentException):
    def __str__(self):
        return "TransactionAmountOverLimit"


class TransactionCountOverLimit(PaymentException):
    def __str__(self):
        return "TransactionCountOverLimit"


class ItsRaining(PaymentException):
    def __str__(self):
        return "ItsRaining"
