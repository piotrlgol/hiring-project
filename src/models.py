from dataclasses import dataclass
from datetime import datetime


@dataclass
class Merchant:
    name: str
    city: str
    lat: float
    lon: float

    def __str__(self):
        return f"{self.name}, {self.city}, {self. lat}, {self. lon}"


@dataclass
class Transaction:
    token: str
    MCC: str
    amount: int
    currency: str
    transaction_time: datetime
    merchant: Merchant

    def __str__(self):
        return f"{self.token}, {self.transaction_time.isoformat()}, {self. amount}, {self. currency}, {self.merchant} \n"
