import csv
from datetime import datetime
from typing import Union
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement

from orinoco.entities import ActionConfig, Signature
from orinoco.typed_action import TypedAction

from custom_exceptions import PaymentException
from db import load_db, save_transaction_event
from models import Merchant, Transaction
from weather_prediction.weather_prediction import DummyWeatherModel

COLD_TEMP = 10

DEFAULT_TRANSACTION_COUNT_LIMIT = 1
SUNNY_DAY_TRANSACTION_COUNT_LIMIT = 2

DEFAULT_TRANSACTION_AMOUNT_LIMIT = 150
COLD_DAY_TRANSACTION_AMOUNT_LIMIT = DEFAULT_TRANSACTION_AMOUNT_LIMIT * 0.5


class GetWeatherInformation(TypedAction):
    CONFIG = ActionConfig(
        INPUT={
            "date": Signature(key="date"),
            "city": Signature(key="city"),
        },
        OUTPUT=Signature(key="weather_information"),
    )

    def __call__(self, date, city):
        return DummyWeatherModel().get_weather(date, city)


def parse_request(payload: str) -> Transaction:
    try:
        et = ElementTree.fromstring(payload)
        return Transaction(
            token=et.find("Transaction").find("Token").text,
            MCC=et.find("Transaction").find("MCC").text,
            amount=int(et.find("Transaction").find("Amount").text),
            currency=et.find("Transaction").find("Currency").text,
            transaction_time=datetime.fromisoformat(
                et.find("Transaction")
                .find("Transaction_Time")
                .text.replace(",", ".")[:-2]
                + ":"
                + et.find("Transaction").find("Transaction_Time").text[-2:]
            ),
            merchant=Merchant(
                name=et.find("Transaction").find("Merchant").find("Name").text,
                city=et.find("Transaction").find("Merchant").find("Merchant_City").text,
                lat=float(
                    et.find("Transaction")
                    .find("Merchant")
                    .find("Location")
                    .find("Lat")
                    .text
                ),
                lon=float(
                    et.find("Transaction")
                    .find("Merchant")
                    .find("Location")
                    .find("Lon")
                    .text
                ),
            ),
        )
    except AttributeError:
        raise PaymentException()


def charge(transaction: Transaction, db_query: dict) -> None:
    data = load_db()
    data.at[db_query["index"], "Limit"] = (
        int(db_query["data"]["Limit"]) - transaction.amount
    )
    save_transaction_event(transaction)
    data.to_csv(
        "../src/resources/db", na_rep="null", index=False, quoting=csv.QUOTE_ALL
    )


def send_response(result: str = "ACCEPTED", reason: str = "None") -> str:
    body = Element("Body")
    transaction = SubElement(body, "TransactionResponse")
    result_xml = SubElement(transaction, "Result")
    result_xml.text = result
    reason_xml = SubElement(transaction, "Reason")
    reason_xml.text = reason
    return ElementTree.tostring(body, encoding="utf8", method="xml")


def adjust_transaction_count_limit_based_on_weather(weather: dict) -> int:
    if weather["clouds"] == "SUNNY":
        return SUNNY_DAY_TRANSACTION_COUNT_LIMIT
    return DEFAULT_TRANSACTION_COUNT_LIMIT


def adjust_single_transaction_limit_based_on_weather(
    weather: dict,
) -> Union[int, float]:
    if weather["temperature"] < COLD_TEMP:
        return COLD_DAY_TRANSACTION_AMOUNT_LIMIT
    return DEFAULT_TRANSACTION_AMOUNT_LIMIT


def wind_from_N_to_S(weather: dict) -> bool:
    if weather["wind_direction"] == "N":
        return True
    return False
