from actions import (adjust_single_transaction_limit_based_on_weather,
                     adjust_transaction_count_limit_based_on_weather, charge,
                     parse_request, send_response, wind_from_N_to_S)
from custom_exceptions import PaymentException
from db import load_db, query_bank, query_card
from validators import (validate_amount, validate_raining,
                        validate_single_transaction_limit,
                        validate_transaction_count)
from weather_prediction.weather_prediction import DummyWeatherModel


class PaymentServer:
    def handle(self, payment_message_request_xml: str) -> str:
        try:
            transaction = parse_request(payment_message_request_xml)
            weather = DummyWeatherModel().get_weather(
                transaction.transaction_time.date(), transaction.merchant.city
            )
            if wind_from_N_to_S(weather):
                db_query = query_bank(load_db())
            else:
                db_query = query_card(transaction, load_db())
            validate_amount(transaction, db_query["data"])
            validate_single_transaction_limit(
                transaction,
                transaction_limit=adjust_single_transaction_limit_based_on_weather(
                    weather
                ),
            )
            validate_transaction_count(
                transaction,
                transaction_limit=adjust_transaction_count_limit_based_on_weather(
                    weather
                ),
            )
            validate_raining(weather)
            charge(transaction, db_query)
            return send_response()
        except PaymentException as exception:
            return send_response("DECLINED", str(exception))
