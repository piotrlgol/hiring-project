import csv
from datetime import datetime
from typing import Union

import pandas as pd
from pandas import DataFrame, Series

from models import Transaction


def load_db() -> DataFrame:
    with open("../src/resources/db", "r+") as db:
        return pd.read_csv(
            db,
            delimiter=",",
            quotechar='"',
            skipinitialspace=True,
            dtype={
                "Limit": int,
                "account": str,
                "currency": str,
                "owner": str,
                "card_token": str,
            },
        )


def query_card(
    transaction: Transaction, data: DataFrame
) -> dict[str, Union[int, Series]]:
    index = next(
        index
        for index, row in enumerate(data["card_token"])
        if row == transaction.token
    )
    return {"index": index, "data": data.iloc[index, :]}


def query_bank(data: DataFrame) -> dict[str, Union[int, Series]]:
    index = next(index for index, row in enumerate(data["owner"]) if row == "bank")
    return {"index": index, "data": data.iloc[index, :]}


def query_recent_transactions(transaction: Transaction) -> list:
    with open("../src/resources/transactions.csv", "r+") as db:
        data = csv.reader(
            db,
            delimiter=",",
            skipinitialspace=True,
        )
        return [
            row
            for row in data
            if (
                row[0] == transaction.token
                and datetime.fromisoformat(row[1]).date()
                == transaction.transaction_time.date()
            )
        ]


def save_transaction_event(transaction: Transaction) -> None:
    with open("../src/resources/transactions.csv", "a") as transactions_db:
        transactions_db.write(str(transaction))
