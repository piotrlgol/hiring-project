from pandas import Series

from custom_exceptions import (InsufficientFunds, ItsRaining,
                               TransactionAmountOverLimit,
                               TransactionCountOverLimit)
from db import query_recent_transactions
from models import Transaction


def validate_amount(transaction: Transaction, db_row: Series) -> None:
    if transaction.amount > db_row["Limit"]:
        raise InsufficientFunds()


def validate_transaction_count(
    transaction: Transaction, transaction_limit: int
) -> None:
    recent_transactions = query_recent_transactions(transaction)
    if not len(recent_transactions) < transaction_limit:
        raise TransactionCountOverLimit()


def validate_single_transaction_limit(
    transaction: Transaction, transaction_limit: int
) -> None:
    if transaction.amount > transaction_limit:
        raise TransactionAmountOverLimit()


def validate_raining(weather: dict) -> None:
    if weather["clouds"] == "RAINING":
        raise ItsRaining()
